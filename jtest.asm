.disk [filename="joytest.d64"]{}

// 10 SYS (4096):REM by james o'hara

*=$0801

        .byte    $20, $04, $0A, $00, $9E, $20, $28,  $34, $30, $39, $36, $29, $3a, $8f, $20, $42, $59, $20, $4A, $41, $4D, $45, $53, $20, $4F, $27, $48, $41, $52, $41, $00, $00, $00

*=$1000
.var screen = $0400
.var chrin = $ffe4
.var chrout = $ffd2
.var clscreen = $e544
.var bordercol = $d020
.var bgcol = $d021
.var txtcol = $286
.var cursx = $d3
.var cursy = $d6
.var movecurs = $e56c
.var jport2reg = $dc00
.var jport1reg = $dc01

start:
        jsr titlescreen
mainloop:
        jsr testport2
        jsr testport1
        jsr chrin
        cmp #81
        bne mainloop
        jsr clscreen
        jsr saygoodbye
        rts

titlescreen:
        lda #00
        sta bordercol
        sta bgcol
        lda # 7
        sta txtcol
        jsr clscreen
        lda #00
        ldx #00
!loop:
        lda title1,x
        jsr chrout
        inx
        cmp #00
        bne !loop-
        ldx #00
!loop:
        lda title2,x
        jsr chrout
        inx
        cmp #00
        bne !loop-
        rts

saygoodbye:
        lda #00
        sta bordercol
        sta bgcol
        lda # 7
        sta txtcol
        jsr clscreen
        lda #00
        ldx #00
!loop:
        lda goodbye,x
        jsr chrout
        inx
        cmp #00
        bne !loop-
        rts

testport2:
        lda #8
        sta textrow
        jsr whitecursor
        jsr testp2up
        jsr printup
        jsr whitecursor
        jsr testp2down
        jsr printdown
        jsr whitecursor
        jsr testp2left
        jsr printleft
        jsr whitecursor
        jsr testp2right
        jsr printright
        jsr whitecursor
        jsr testp2fire
        jsr printfire
        rts

testport1:
        lda #12
        sta textrow
        jsr whitecursor
        jsr testp1up
        jsr printup
        jsr whitecursor
        jsr testp1down
        jsr printdown
        jsr whitecursor
        jsr testp1left
        jsr printleft
        jsr whitecursor
        jsr testp1right
        jsr printright
        jsr whitecursor
        jsr testp1fire
        jsr printfire
        rts

printup:
        lda #2
        sta cursx
        lda textrow
        sta cursy
        jsr movecurs
        lda #0
        ldx #0
!loop:
        lda up, x
        jsr chrout
        inx
        cmp #00
        bne !loop-
        rts

printdown:
        lda #6
        sta cursx
        lda textrow
        sta cursy
        jsr movecurs
        lda #0
        ldx #0
!loop:
        lda down, x
        jsr chrout
        inx
        cmp #00
        bne !loop-
        rts

printleft:
        lda #12
        sta cursx
        lda textrow
        sta cursy
        jsr movecurs
        lda #0
        ldx #0
!loop:
        lda left, x
        jsr chrout
        inx
        cmp #00
        bne !loop-
        rts

printright:
        lda #18
        sta cursx
        lda textrow
        sta cursy
        jsr movecurs
        lda #0
        ldx #0
!loop:
        lda right, x
        jsr chrout
        inx
        cmp #00
        bne !loop-
        rts

printfire:
        lda #25
        sta cursx
        lda textrow
        sta cursy
        jsr movecurs
        lda #0
        ldx #0
!loop:
        lda fire, x
        jsr chrout
        inx
        cmp #00
        bne !loop-
        rts

whitecursor:
        lda #01
        sta txtcol
        rts
redcursor:
        lda #02
        sta txtcol
        rts

testp2up:
        lda jport2reg
        and #01
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts

testp2down:
        lda jport2reg
        and #02
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts

testp2left:
        lda jport2reg
        and #04
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts

testp2right:
        lda jport2reg
        and #08
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts

testp2fire:
        lda jport2reg
        and #16
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts

// Port 1
testp1up:
        lda jport1reg
        and #01
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts

testp1down:
        lda jport1reg
        and #02
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts

testp1left:
        lda jport1reg
        and #04
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts

testp1right:
        lda jport1reg
        and #08
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts

testp1fire:
        lda jport1reg
        and #16
        cmp #00
        bne !quittest+
        jsr redcursor
!quittest:
        rts


title1:
        .byte 18
        .text "          JOYSTICK TESTER               "
        .byte 13
        .text "BY JAMES O'HARA "
        .byte 64
        .text "JIMJIM76 2019"
        .byte 13,13
        .text "PRESS "
        .byte 18,81,146
        .text " TO QUIT"
        .byte 13,13,13,0
title2:
        .byte 32,117,99,99,99,99,99,99,99,99,99,80,79,82,84,99,50,99,99,99,99,99,99,99,99,99,99,99,99,105,13
        .byte 32,98,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,98,13
        .byte 32,106,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,107,13
        .byte 13
        .byte 32,117,99,99,99,99,99,99,99,99,99,80,79,82,84,99,49,99,99,99,99,99,99,99,99,99,99,99,99,105,13
        .byte 32,98,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,32,98,13
        .byte 32,106,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,99,107,13
 
        .byte 0

goodbye:
        .text "THANK YOU FOR USING THIS UTIL"
        .byte 13,13
        .text "HTTPS://GITLAB.COM/DJVULCAN"
        .byte 13,13
        .byte 0

up:
        .text "UP"
        .byte 0
down:
        .text "DOWN"
        .byte 0
left:
        .text "LEFT"
        .byte 0
right:
        .text "RIGHT"
        .byte 0
fire:
        .text "FIRE"
        .byte 0

textrow:
        .byte 0